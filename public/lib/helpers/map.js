function MapClass(){
	var layer = new Kinetic.Layer(),
		fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		callbacks=[],
		group,image;
	function init(){
		layer.draw();
		for(var i=0;i<callbacks.length;i++){
			callbacks[i]();
		}
	}
	group = new Kinetic.Group({
		x:width/2-250,
		y:height/2-207,
		width:500,
		height:414,
		opacity:0
	});
	layer.clear();
	layer.add(group);
	var loader = new LoaderClass({map:{type:'map',key:1}});
	loader.complete(function(images){
		image = new Kinetic.Image({
			y:0,
			x:0,
			image:images.map,
			padding:0
		});
		group.add(image);
		init();
	});
	new button(layer,{
		x:width-100,
		y:0,
		width:100,
		text:'Map',
		click:function(){
			//init map
			console.log(group.getOpacity());
			if(group.getOpacity()==0){
				group.setOpacity(1);
			}else{
				group.setOpacity(0);
			}
			layer.draw();
		}
	});
	this.getLayer = function(){
		return layer;
	}
	this.ready = function(callback){
		callbacks.push(callback);
	}
	return this;
}