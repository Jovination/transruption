function dialog(title,msg,yes){
	var fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		group = new Kinetic.Group({
			x:(width/2)-(config.dialog.width/2),
			y:(height/2)-(config.dialog.height/2)
		}),
		layer = new Kinetic.Layer(),
		modal = new Kinetic.Rect({
			width:config.dialog.width,
			height:config.dialog.height,
			fill:config.dialog.fill,
			stroke:config.dialog.stroke,
			strokeWidth:config.dialog.strokeWidth,
			cornerRadius:config.dialog.cornerRadius
		}),
		modalBack = new Kinetic.Rect({
			width:width,
			height:height,
			fill:config.dialog.backFill,
			opacity:config.dialog.backOpacity
		}),
		dialogTitle = new Kinetic.Text({
			x:5,
			y:10,
			text:title,
			width:config.dialog.width,
			fontSize:config.dialog.titleSize,
			fontFamily:config.dialog.fontFamily,
			fill:config.dialog.color
		}),
		dialogMsg = new Kinetic.Text({
			x:5,
			y:dialogTitle.getHeight()+10,
			text:msg,
			width:config.dialog.width,
			padding:5,
			fontSize:config.dialog.msgSize,
			fontFamily:config.dialog.fontFamily,
			fill:config.dialog.color
		});

	layer.add(modalBack);
	group.add(modal);
	group.add(dialogTitle);
	group.add(dialogMsg);
	layer.add(group);

	new button(group,{
		x:5,
		y:config.dialog.height-35,
		width:(config.dialog.width/2)-10,
		text:config.dialog.yes,
		click:yes
	});
	new button(group,{
		x:(config.dialog.width/2)+5,
		y:config.dialog.height-35,
		width:(config.dialog.width/2)-10,
		text:config.dialog.no,
		click:function(){
			layer.remove()
		}
	});
	window.stage.add(layer);
}