function NpcClass(){
	var layer = new Kinetic.Layer(),
		fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		callbacks=[],
		npc,exp,group;
	function init(){
		layer.draw();
		for(var i=0;i<callbacks.length;i++){
			callbacks[i]();
		}
	}
	this.set = function(key,key2){
		layer.clear();
		var loader = new LoaderClass({npc:{type:'npc',key:key},exp:{type:'expression',key:key2}});
		loader.complete(function(images){
			npc = new Kinetic.Image({
				y:0,
				x:0,
				image:images.npc,
				padding:0
			});
			exp = new Kinetic.Image({
				y:0,
				x:0,
				image:images.exp,
				padding:0
			});
			group = new Kinetic.Group({
				x:(width/2)-(npc.getWidth()/2),
				y:height-npc.getHeight(),
				width:npc.getWidth(),
				height:npc.getHeight()
			});
			group.add(npc).add(exp);
			layer.add(group);
			init();
		});
	}
	this.getLayer = function(){
		return layer;
	}
	this.ready = function(callback){
		callbacks.push(callback);
	}
	return this;
}