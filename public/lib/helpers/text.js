function TextClass(){
	var layer = new Kinetic.Layer(),
		fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		callbacks=[],
		group,text,rect;
	function init(){
		layer.draw();
		for(var i=0;i<callbacks.length;i++){
			callbacks[i]();
		}
	}
	this.set = function(data){
		text.setText(data);
		layer.draw();
	}
	this.getLayer = function(){
		return layer;
	}
	group = new Kinetic.Group({
		x:50,
		y:height-250,
		width:width-100,
		height:200
	});
	rect = new Kinetic.Rect({
		x:0,
		y:0,
		width:group.getWidth(),
		height:group.getHeight(),
		opacity:0.7,
		fill:'#000'
	});
	text = new Kinetic.Text({
		x:0,
		y:0,
		width:group.getWidth(),
		height:group.getHeight(),
		fill:'#fff',
		fontFamily:'Calibri',
		fontSize:16,
		text:'You wake up feeling drowsy. As you shift you feel the hard rough ground underneath you. {{i}}"Did I fall off my bed?"{{ei}} you wonder to yourself. While you continue to shift around you notice that you can feel the sun\'s heat on your skin. How did I end up outside?" you mutter finally opening your eyes. It takes a second for your eyes to adjust, once they do you gasp loudly.',
		padding:20
	});
	group.add(rect);
	group.add(text);
	layer.add(group);
	init();
}