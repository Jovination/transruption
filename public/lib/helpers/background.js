function BackgroundClass(){
	var layer = new Kinetic.Layer(),
		fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		callbacks=[],
		image;
	function init(){
		layer.draw();
		for(var i=0;i<callbacks.length;i++){
			callbacks[i]();
		}
	}
	this.set = function(key){
		layer.clear();
		var loader = new LoaderClass({background:{type:'background',key:key}});
		loader.complete(function(images){
			image = new Kinetic.Image({
				y:0,
				x:0,
				image:images.background,
				padding:0
			});
			layer.add(image);
			init();
		});
	}
	this.getLayer = function(){
		return layer;
	}
	this.ready = function(callback){
		callbacks.push(callback);
	}
	return this;
}