function FormsClass(parent,layer){
	var fn = this,
		lastY = 0,
		listeners = {};

	this.states = {};
	this.draw = function(){
		return layer.draw();
	}
	this.naming = function(images,params){
		//globals
		var cont,inputBack,male,female,futa,tallyX,items={};

		//setup defaults
		var params = params || {};
		params.x = params.x || config.forms.naming.defaults.x;
		params.y = params.y || lastY;
		params.radius = params.radius || config.forms.naming.defaults.radius;
		params.active = params.active || 0;
		params.height = params.height || config.forms.naming.defaults.height;
		params.width = params.width || parent.getWidth();
		params.margins = params.margins || config.forms.naming.defaults.margins;

		lastY += params.height;

		var defaults = {
			active:'male',
			input:'noname'
		};

		//container for naming area
		cont = new Kinetic.Group({
			x:params.x,
			y:params.y,
			height:params.height,
			width:params.width
		});
		//keep a tally of X to make sure everything is inline
		tallyX = params.margins;
		inputBack = new Kinetic.Rect({
			x:params.margins,
			y:params.margins,
			width:(params.width-(params.margins*2))-(3*(params.radius*2)+(params.margins*3)),
			height:params.height-params.margins,
			fill:'red',
			lineJoin:config.forms.naming.inputLineJoin,
			stroke:config.forms.naming.inputStroke,
			strokeWidth:config.forms.naming.inputStrokeWidth,
			lineCap:config.forms.naming.inputLineCap,
			fillLinearGradientStartPoint:config.forms.naming.inputGradient.startPoint,
			fillLinearGradientEndPoint:config.forms.naming.inputGradient.endPoint(params.height-params.margins),
			fillLinearGradientColorStops:config.forms.naming.inputGradient.colorStops,
			fillPriority:config.forms.naming.inputGradient.priority
		});
		var inputElement = jQuery('<input/>',{
			id:'name-select',
			type:'text'
		}).css({
			left:(parent.getX()+(params.x+params.margins))+'px',
			top:(params.y+params.margins)+'px',
			height:params.height-params.margins+'px',
			width:inputBack.getWidth()-10+'px',
			color:config.forms.naming.textColor
		});
		jQuery('#container').append(inputElement);

		tallyX += inputBack.getWidth()+params.margins;
		items.male = {};
		items.male.group = new Kinetic.Group({
			x:tallyX+params.radius,
			y:params.margins+params.radius,
		});
		items.male.back = new Kinetic.Circle({
			radius:params.radius,
			fill:config.forms.naming.genderFill,
			stroke:config.forms.naming.genderStroke,
			strokeWidth:2,
			fillLinearGradientStartPoint:config.forms.naming.genderGradient.startPoint,
			fillLinearGradientEndPoint:config.forms.naming.genderGradient.endPoint(params.radius),
			fillLinearGradientColorStops:config.forms.naming.genderGradient.colorStops,
			fillPriority:config.forms.naming.genderGradient.priority
		});
		items.male.label = new Kinetic.Image({
			y:-params.radius,
			x:-params.radius,
			width:params.radius*2,
			image:images.male,
			padding:0
		});
		items.male.group
			.add(items.male.back)
			.add(items.male.label);

		tallyX += (params.radius*2)+params.margins;
		items.female = {};
		items.female.group = new Kinetic.Group({
			x:tallyX+params.radius,
			y:params.margins+params.radius,
		});
		items.female.back = new Kinetic.Circle({
			radius:params.radius,
			fill:config.forms.naming.genderFill,
			stroke:config.forms.naming.genderFill,
			strokeWidth:null,
			fillLinearGradientStartPoint:config.forms.naming.genderGradient.startPoint,
			fillLinearGradientEndPoint:config.forms.naming.genderGradient.endPoint(params.radius),
			fillLinearGradientColorStops:config.forms.naming.genderGradient.colorStops,
			fillPriority:config.forms.naming.genderGradient.priority
		});
		items.female.label = new Kinetic.Image({
			y:-params.radius,
			x:-params.radius,
			width:params.radius*2,
			image:images.female
		});
		items.female.group
			.add(items.female.back)
			.add(items.female.label);

		tallyX += (params.radius*2)+params.margins;
		items.futa = {};
		items.futa.group = new Kinetic.Group({
			x:tallyX+params.radius,
			y:params.margins+params.radius,
		});
		items.futa.back = new Kinetic.Circle({
			radius:params.radius,
			fill:config.forms.naming.genderFill,
			stroke:config.forms.naming.genderFill,
			strokeWidth:null,
			fillLinearGradientStartPoint:config.forms.naming.genderGradient.startPoint,
			fillLinearGradientEndPoint:config.forms.naming.genderGradient.endPoint(params.radius),
			fillLinearGradientColorStops:config.forms.naming.genderGradient.colorStops,
			fillPriority:config.forms.naming.genderGradient.priority
		});
		items.futa.label = new Kinetic.Image({
			y:-params.radius,
			x:-params.radius,
			width:params.radius*2,
			image:images.futa
		});
		items.futa.group
			.add(items.futa.back)
			.add(items.futa.label);

		cont
			.add(inputBack)
			.add(items.male.group)
			.add(items.female.group)
			.add(items.futa.group);
		setNamingState(params.id,defaults);
		fn._setNamingTween(items,params);
		parent.add(cont);
		return fn;
	}
	this.colorSwitch = function(params){
		//globals
		var cont,text,items=[];

		//setup defaults
		var params = params || {};
		params.x = params.x || config.forms.colorSwitch.defaults.x;
		params.y = params.y || lastY;
		params.text = params.text || null;
		params.radius = params.radius || config.forms.colorSwitch.defaults.radius;
		params.active = params.active || 0;
		params.height = params.height || config.forms.colorSwitch.defaults.height;
		params.width = params.width || parent.getWidth();
		params.margins = params.margins || config.forms.colorSwitch.defaults.margins;

		lastY += params.height;

		//container for colorSwitch
		cont = new Kinetic.Group({
			x:params.x,
			y:params.y,
			height:params.height,
			width:params.width
		});

		text = new Kinetic.Text({
			x:params.margins,
			y:params.margins,
			text:params.text,
			fontFamily:config.forms.colorSwitch.fontFamily,
			fontSize:config.forms.colorSwitch.fontSize,
			width:config.forms.colorSwitch.textWidth,
			fill:config.forms.colorSwitch.color
		});
		text.setY((text.getHeight()/2)+params.margins);
		cont.add(text);

		var tallyX = config.forms.colorSwitch.textWidth+params.margins;

		for(var i=0;i<params.children.length;i++){
			items[i] = {};
			items[i].group = new Kinetic.Group({
				x:tallyX+params.radius,
				y:params.margins+params.radius,
			});
			items[i].back = new Kinetic.Circle({
				y:params.margins,
				radius:params.radius,
				fill:params.children[i].fill,
				stroke:(i==params.active?config.forms.colorSwitch.activeStroke:config.forms.colorSwitch.stroke),
				strokeWidth:config.forms.colorSwitch.strokeWidth
			});
			items[i].group.add(items[i].back);
			cont.add(items[i].group);
			tallyX += (params.radius*2)+params.margins;
		}
		setColorSwitchState(params.id,params.active);
		fn._setColorSwitchTween(items,params);
		parent.add(cont);
		return fn;
	};
	this.style = function(params){
		//globals
		var cont,items=[];

		//setup defaults
		var params = params || {};
		params.x = params.x || config.forms.style.defaults.x;
		params.y = params.y || lastY;
		params.text = params.text || null;
		params.radius = params.radius || config.forms.style.defaults.radius;
		params.active = params.active || 0;
		params.height = params.height || config.forms.style.defaults.height;
		params.width = params.width || parent.getWidth();
		params.margins = params.margins || config.forms.style.defaults.margins;

		lastY += params.height;

		//container for style
		cont = new Kinetic.Group({
			x:params.x,
			y:params.y,
			height:params.height
		});

		var tallyX = params.margins,calcWidth;

		for(var i=0;i<params.children.length;i++){
			items[i] = {};
			items[i].group = new Kinetic.Group({
				x:tallyX+params.radius,
				y:params.margins+params.radius,
			});
			items[i].back = new Kinetic.Circle({
				y:params.margins,
				radius:params.radius,
				fill:params.children[i].fill,
				stroke:(i==params.active?config.forms.style.activeStroke:config.forms.style.stroke),
				strokeWidth:config.forms.style.strokeWidth
			});
			items[i].group.add(items[i].back);
			cont.add(items[i].group);
			tallyX += (params.radius*2)+params.margins;
		}
		calcWidth = ((params.radius*2)*params.children.length)+(params.margins*params.children.length);
		setStyleState(params.id,params.active);
		fn._setStyleTween(items,params);
		cont.setX((parent.getWidth()/2)-(calcWidth/2));
		parent.add(cont);
		return fn;
	}
	this.selector = function(params){
		//globals
		var cont,obj,lastWidth=0,items=[],group,text;

		//setup defaults
		var params = params || {};
		params.x = params.x || config.forms.selector.defaults.x;
		params.y = params.y || lastY;
		params.active = params.active || 0;
		params.height = params.height || config.forms.selector.defaults.height;
		params.children = params.children || [{
			label:'test',
			text:'lorem'
		}];
		params.width = params.width || parent.getWidth();
		params.margins = params.margins || config.forms.selector.defaults.margins;

		lastY += params.height;

		//container for selector
		cont = new Kinetic.Group({
			x:params.x,
			y:params.y,
			height:params.height,
			width:params.width
		});
		//air.Introspector.Console.log(params);

		var totalMargins = params.margins*(params.children.length+1),
			activeWidth = (params.width-totalMargins)/2+1,
			subItems = params.children.length-1,
			subWidth = Math.floor((activeWidth-1)/subItems),
			itemHeight = (params.height-params.margins)/2;
		//air.trace(activeWidth,subWidth);
		//loop all children
		for(var i=0;i<params.children.length;i++){
			//use an array to store objects
			items[i]={};
			//create default object for rectangle
			obj = {};
			text = {x:0,y:itemHeight+params.margins};
			group = {};
			group.y = params.margins;
			obj.y = 0;
			obj.fill = params.children[i].fill || config.forms.selector.buttonFill;
			if(params.active==i){
				obj.width = activeWidth;
				group.width = activeWidth;
				text.visible = true;
			}else{
				obj.width = subWidth;
				group.width = subWidth;
				text.visible = false;
			}
			obj.lineJoin = config.forms.selector.lineJoin;
			obj.stroke = config.forms.selector.stroke;
			obj.strokeWidth = config.forms.selector.strokeWidth;
			obj.lineCap = config.forms.selector.lineCap;
			obj.fillLinearGradientStartPoint = config.forms.selector.gradient.fillLinearGradientStartPoint;
			obj.fillLinearGradientEndPoint = config.forms.selector.gradient.fillLinearGradientEndPoint((params.height-itemHeight)-(params.margins*2));
			obj.fillLinearGradientColorStops = config.forms.selector.gradient.fillLinearGradientColorStops;
			obj.fillPriority = config.forms.selector.gradient.fillPriority;
			group.x = params.margins+lastWidth;
			obj.height = itemHeight;
			lastWidth = lastWidth+(params.margins+obj.width);
			//create group for text
			items[i].text = new Kinetic.Group(text);
			items[i].label = new Kinetic.Text({
				text:params.children[i].label,
				fontFamily: config.forms.selector.fontFamily,
				fontSize:30,
				width:(params.active==i?activeWidth:subWidth),
				align:'center',
				fill:config.forms.selector.color
			});
			items[i].label.setY(((itemHeight/2)-(items[i].label.getHeight()))+params.margins);
			items[i].body = new Kinetic.Text({
				x:params.margins,
				y:params.margins,
				text:params.children[i].text,
				fontFamily: config.forms.selector.fontFamilyBody,
				fontSize:14,
				fill:config.forms.selector.color,
				align:'center',
				width:params.width-(params.margins*2),
				height:(params.height-itemHeight)-(params.margins*2),
				padding:20
			});
			//create box for text
			items[i].box = new Kinetic.Rect({
				x: params.margins,
				y: params.margins,
				width:params.width-(params.margins*2),
				height:(params.height-itemHeight)-(params.margins*2),
				fill:params.children[i].fill || config.forms.selector.buttonFill,
				lineJoin:config.forms.selector.lineJoin,
				stroke:config.forms.selector.stroke,
				strokeWidth:config.forms.selector.strokeWidth,
				lineCap:config.forms.selector.lineCap,
				fillLinearGradientStartPoint:config.forms.selector.gradient.fillLinearGradientStartPoint,
				fillLinearGradientEndPoint:config.forms.selector.gradient.fillLinearGradientEndPoint((params.height-itemHeight)-(params.margins*2)),
				fillLinearGradientColorStops:config.forms.selector.gradient.fillLinearGradientColorStops,
				fillPriority:config.forms.selector.gradient.fillPriority
			});
			//air.Introspector.Console.log(items[i].box);
			//add box to text groups
			items[i].text.add(items[i].box);
			items[i].text.add(items[i].body);
			//create item
			items[i].group = new Kinetic.Group(group);
			items[i].item = new Kinetic.Rect(obj);
			items[i].group.add(items[i].item);
			items[i].group.add(items[i].label);
			//add groups to parent
			cont
				.add(items[i].group)
				.add(items[i].text);
		}
		setSelectorState(params.id,params.active);
		fn._setSelectorTween(items,params);
		//add parent to the bound container
		parent.add(cont);
		return fn;
	};
	this.on = function(name,callback){
		switch(name){
			case 'change':
				_setChangeEvent(callback);
				break;
		}
		return fn;
	}
	this._setNamingTween = function(items,params){
		for(var key in items){
			if(items.hasOwnProperty(key)){
				(function(_i){
					items[_i].group.on('mouseover',function(){
						document.body.style.cursor = 'pointer';
					});
					items[_i].group.on('mouseout',function(){
						document.body.style.cursor = 'default';
					});
					items[_i].group.on('click',function(){
						for(var key in items){
							if(items.hasOwnProperty(key)){
								if(_i==key){
									items[key].back.setStroke(config.forms.naming.genderStroke);
									items[key].back.setStrokeWidth(2);
								}else{
									items[key].back.setStroke(config.forms.naming.genderFill);
									items[key].back.setStrokeWidth(null);
								}
							}
						}
						setNamingState(params.id,{
							active:_i,
							input:jQuery('#name-select').val()
						});
						_fireEvent('change',fn.states);
						fn.draw();
					});
				})(key);
			}
		}
	}
	this._setColorSwitchTween = function(items,params){
		for(var key in items){
			if(items.hasOwnProperty(key)){
				(function(_i){
					items[_i].group.on('mouseover',function(){
						document.body.style.cursor = 'pointer';
					});
					items[_i].group.on('mouseout',function(){
						document.body.style.cursor = 'default';
					});
					items[_i].group.on('click',function(){
						for(var key in items){
							if(items.hasOwnProperty(key)){
								if(_i==key){
									items[key].back.setStroke(config.forms.colorSwitch.activeStroke);
								}else{
									items[key].back.setStroke(config.forms.colorSwitch.stroke);
								}
							}
						}
						setColorSwitchState(params.id,_i);
						_fireEvent('change',fn.states);
						fn.draw();
					});
				})(key);
			}
		}
	}
	this._setStyleTween = function(items,params){
		for(var key in items){
			if(items.hasOwnProperty(key)){
				(function(_i){
					items[_i].group.on('mouseover',function(){
						document.body.style.cursor = 'pointer';
					});
					items[_i].group.on('mouseout',function(){
						document.body.style.cursor = 'default';
					});
					items[_i].group.on('click',function(){
						for(var key in items){
							if(items.hasOwnProperty(key)){
								if(_i==key){
									items[key].back.setStroke(config.forms.style.activeStroke);
								}else{
									items[key].back.setStroke(config.forms.style.stroke);
								}
							}
						}
						setStyleState(params.id,_i);
						_fireEvent('change',fn.states);
						fn.draw();
					});
				})(key);
			}
		}
	}
	this._setSelectorTween = function(items,params){
		for(var i=0;i<items.length;i++){
			(function(_i){
				items[_i].group.on('mouseover',function(){
					document.body.style.cursor = 'pointer';
				});
				items[_i].group.on('mouseout',function(){
					document.body.style.cursor = 'default';
				});
				items[_i].group.on('click',function(){
					fn._setSelectorEvent(items,_i,params);
				});
			})(i);
		}
	}
	this._setSelectorEvent = function(items,_i,params){
		var totalMargins = params.margins*(params.children.length+1),
			activeWidth = (params.width-totalMargins)/2+1,
			subItems = params.children.length-1,
			subWidth = Math.floor((activeWidth-1)/subItems),
			itemHeight = (params.height-params.margins)/2;
		var tween,obj,group,lastWidth=0,label;
		for(var i=0;i<items.length;i++){
			obj = {onUpdate:fn.draw,onUpdateScope:fn};
			group = {onUpdate:fn.draw,onUpdateScope:fn};
			label = {onUpdate:fn.draw,onUpdateScope:fn};
			if(_i==i){
				obj.setWidth = activeWidth;
				group.setWidth = activeWidth;
				label.setWidth = activeWidth;
				items[i].text.show();
			}else{
				obj.setWidth = subWidth;
				group.setWidth = subWidth;
				label.setWidth = subWidth;
				items[i].text.hide();
			}
			group.setX = params.margins+lastWidth;
			lastWidth = lastWidth+(params.margins+obj.setWidth);

			items[i].group.off('click');
			obj.onComplete = (function(items,_i,params){
				return function(){
					items[_i].group.on('click',function(){
						fn._setSelectorEvent(items,_i,params);
					});
				}
			})(items,i,params);
			TweenMax.to(items[i].group,config.forms.selector.tweenDuration,group);
			TweenMax.to(items[i].item,config.forms.selector.tweenDuration,obj);
			TweenMax.to(items[i].label,config.forms.selector.tweenDuration,label);
		}
		setSelectorState(params.id,_i);
		_fireEvent('change',fn.states);
	}
	function setSelectorState(id,active){
		if(!fn.states.selector){
			fn.states.selector = {};
		}
		fn.states.selector[id] = parseInt(active);
	}
	function setNamingState(id,obj){
		if(!fn.states.naming){
			fn.states.naming = {};
		}
		fn.states.naming[id] = obj;
	}
	function setColorSwitchState(id,active){
		if(!fn.states.colorswitch){
			fn.states.colorswitch = {};
		}
		fn.states.colorswitch[id] = parseInt(active);
	}
	function setStyleState(id,active){
		if(!fn.states.style){
			fn.states.style = {};
		}
		fn.states.style[id] = parseInt(active);
	}
	function _fireEvent(type,data){
		if(listeners[type]){
			for(var i=0;i<listeners[type].length;i++){
				listeners[type][i](data);
			}
		}
	}
	function _setChangeEvent(callback){
		if(!listeners.change){
			listeners.change = [];
		}
		listeners.change.push(callback);
	}
	this.ready = function(callback){
		callback(fn);
	}
}