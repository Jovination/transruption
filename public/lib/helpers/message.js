function message(title,msg){
	var fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		layer = new Kinetic.Layer(),
		group = new Kinetic.Group({
			x:(width/2)-(config.message.width/2),
			y:(height/2)-(config.message.height/2)
		}),
		modal = new Kinetic.Rect({
			x:0,
			y:0,
			width:config.message.width,
			height:config.message.height,
			fill:config.message.fill,
			stroke:config.message.stroke,
			strokeWidth:config.message.strokeWidth,
			cornerRadius:config.message.cornerRadius
		}),
		modalBack = new Kinetic.Rect({
			x:0,
			y:0,
			width:width,
			height:height,
			fill:config.message.backFill,
			opacity:config.message.backOpacity
		}),
		messageTitle = new Kinetic.Text({
			x:5,
			y:10,
			text:title,
			width:config.message.width,
			fontSize:config.message.titleSize,
			fontFamily:config.message.fontFamily,
			fill:config.message.color
		}),
		messageMsg = new Kinetic.Text({
			x:5,
			y:messageTitle.getHeight()+10,
			text:msg,
			width:config.message.width,
			padding:5,
			fontSize:config.message.msgSize,
			fontFamily:config.message.fontFamily,
			fill:config.message.color
		});
	layer.add(modalBack);
	group.add(modal);
	group.add(messageTitle);
	group.add(messageMsg);
	layer.add(group);
	new button(group,{
		x:10,
		y:config.dialog.height-35,
		width:config.message.width-20,
		text:config.message.button,
		click:function(){
			layer.remove()
		}
	});
	window.stage.add(layer);
}