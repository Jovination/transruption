function button(bind,params){
	if(!params){
		var params = {};
	}
	if(!params.x){
		params.x = 0
	}
	if(!params.y){
		params.y = 0
	}
	if(!params.text){
		params.text = 'blank';
	}
	if(!params.width){
		params.width = config.button.width;
	}
	var group = new Kinetic.Group({
		x: params.x,
		y: params.y
	});
	var text = new Kinetic.Text({
		x:0,
		y:0,
		text: params.text,
		fontSize:config.button.fontSize,
		fontFamily:config.button.fontFamily,
		fill:config.button.color,
		width:params.width,
		padding:config.button.padding,
		align:config.button.align
	});

	var item = new Kinetic.Rect({
		x: 0,
		y: 0,
		width: text.getWidth(),
		height: text.getHeight(),
		fill:config.button.fill,
		stroke:config.button.stroke,
		strokeWidth:config.button.strokeWidth,
		cornerRadius:config.button.cornerRadius
	});
	group.on('mouseover',function(){
		item.setFill(config.button.hoverFill);
		item.setStroke(config.button.hoverStroke);
		text.setFill(config.button.hoverColor);
		document.body.style.cursor = 'pointer';
		bind.draw();
	});
	group.on('mouseout',function(){
		item.setFill(config.button.fill);
		item.setStroke(config.button.stroke);
		text.setFill(config.button.color);
		document.body.style.cursor = 'default';
		bind.draw();
	});
	if(params.click){
		group.on('click',function(){
			document.body.style.cursor = 'default';
			params.click();
		});
	}
	group.add(item);
	group.add(text);
	bind.add(group);
}