function LoaderClass(obj){
	var fn = this,
		loaded = 0,
		imgObj = {};
	this.assetNum = function(){
	    var size = 0, key;
	    for(key in obj){
	        if(obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};
	var loadImg = function(img){
		var image = new TRImage(obj[img]);
		image.ready(function(data){
			loaded += 1;
			imgObj[img] = data;
		});
	}
	this.complete = function(callback){
		for(var index in obj){
			loadImg(index);
		}
		var interval = setInterval(function(){
			if(loaded==fn.assetNum()){
				clearInterval(interval);
				callback(imgObj);
			}
		},50);
	}
}