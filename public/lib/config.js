//colors
var color1 = '#7F782B',
	color2 = '#FFF7A2',
	color3 = '#FFF056',
	color4 = '#7F7613',
	color5 = '#CCC045';

var textcolor1 = '#191919',
	textcolor2 = '#fff';

//handle shades
function shadeColor(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;  
    G = (G<255)?G:255;  
    B = (B<255)?B:255;  

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

    return "#"+RR+GG+BB;
}
//config
window.config = {
	files:{
		size:104857600
	},
	events:{
		path:'events/'
	},
	button:{
		fontSize:18,
		fontFamily:'Lucida Sans Unicode',
		padding:5,
		align:'center',
		cornerRadius:3,
		strokeWidth:1,
		stroke:color4,
		fill:color3,
		width:360,
		color:textcolor1,
		hoverFill:shadeColor(color3,-10),
		hoverColor:textcolor1,
		hoverStroke:shadeColor(color4,-10)
	},
	dialog:{
		fontFamily:'Lucida Sans Unicode',
		fill:color1,
		stroke:shadeColor(color4,-50),
		cornerRadius:3,
		strokeWidth:1,
		width:300,
		height:100,
		backFill:'#000',
		backOpacity:0.5,
		color:textcolor2,
		titleSize:30,
		msgSize:14,
		yes:'Yes',
		no:'No'
	},
	message:{
		fontFamily:'Lucida Sans Unicode',
		fill:color1,
		stroke:shadeColor(color4,-50),
		cornerRadius:3,
		strokeWidth:1,
		width:300,
		height:100,
		backFill:'#000',
		backOpacity:0.5,
		color:textcolor2,
		titleSize:30,
		msgSize:14,
		button:'Return'
	},
	characterCreation:{
		preview:{
			fill:color2,
			width:300
		},
		settings:{
			fill:color5
		}
	},
	title:{
		background:{
			main:{type:'utility',key:1}
		}
	},
	forms:{
		selector:{
			buttonFill:color3,
			tweenDuration:0.4,
			fontFamily:'Lucida Sans Unicode',
			fontFamilyBody:'Lucida Sans Unicode',
			color:textcolor1,
			stroke:color2,
			strokeWidth:2,
			lineJoin:'miter',
			lineCap:'butt',
			gradient:{
				fillLinearGradientStartPoint:[0,0],
				fillLinearGradientEndPoint:function(value){
					return [0,value];
				},
				fillLinearGradientColorStops:[0,color4,1,shadeColor(color4,-30)],
				fillPriority:'linear-gradient'
			},
			defaults:{
				x:0,
				height:200,
				margins:5
			}
		},
		naming:{
			genderFill:color3,
			genderStroke:shadeColor(color3,-30),
			textColor:textcolor1,
			inputGradient:{
				startPoint:[0,0],
				endPoint:function(value){
					return [0,value];
				},
				colorStops:[0,color4,1,shadeColor(color4,-30)],
				priority:'linear-gradient'
			},
			genderGradient:{
				startPoint:[0,0],
				endPoint:function(value){
					return [0,value];
				},
				colorStops:[0,color4,1,shadeColor(color4,-30)],
				priority:'linear-gradient'
			},
			icons:{
				male:{type:'utility',key:2},
				female:{type:'utility',key:4},
				futa:{type:'utility',key:3},
			},
			inputLineJoin:'miter',
			inputStroke:shadeColor(color3,-30),
			inputStrokeWidth:2,
			inputLineCap:'butt',
			defaults:{
				x:0,
				height:40,
				radius:15,
				margins:5
			}
		},
		colorSwitch:{
			fontFamily:'Lucida Sans Unicode',
			fontSize:18,
			color:textcolor1,
			textWidth:200,
			stroke:color3,
			strokeWidth:2,
			activeStroke:shadeColor(color3,-30),
			defaults:{
				x:0,
				margins:5,
				height:40,
				radius:15
			}
		},
		style:{
			stroke:color3,
			strokeWidth:2,
			activeStroke:shadeColor(color3,-30),
			defaults:{
				x:0,
				margins:5,
				height:80,
				radius:30
			}
		}
	}
}