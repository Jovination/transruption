function SystemClass(){
	var fn = this;
	this.init = function(){
		var menu = new MenuClass();
		window.socket = io.connect('http://game-dev.transruption.com:8080');
		window.filer = new Filer();
		window.db = new DBClass();
		menu.addItem('File');
		menu.addSubItem('File','Return To Title',function(){
			dialog('Are you sure?','Returning to title will lose any unsaved progress',function(){
 				var title = new TitleClass();
				title.display();
 			});
		});
		menu.addSubItem('File','New Game',function(){
 			message('Feature not yet available','This feature will be added soon, be sure to check back');
		});
		menu.addSubItem('File','Load Game',function(){
			message('Feature not yet available','This feature will be added soon, be sure to check back');
		});
		filer.init({persistent:true,size:config.files.size},function(fs){
			filer.ls('/', function(entries){
				var list = [];
				for(var i=0;i<entries.length;i++){
					list.push(entries[i].fullPath);
				}
				if(list.indexOf('/images')===-1){
					filer.mkdir('images',false,function(){
						menu.init();
						var title = new TitleClass();
						title.display();
					});
				}else{
					menu.init();
					var title = new TitleClass();
					title.display();
				}
			});
			
		});
	};
	this.clear = function(){
		jQuery('#name-select').remove();
	}
	return this;
}