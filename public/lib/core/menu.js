function MenuClass(){
	var fn = this,
		items = {},
		callbacks = {};
	this.addItem = function(name){
		var item = jQuery('<li/>');
		item.append('<span class="title">'+name+'</span><ul></ul>');
		items[name] = item;
		callbacks[name] = [];
	}
	this.addSubItem = function(index,name,callback){
		var item = jQuery('<li/>');
		item.append('<span class="title">'+name+'</span>');
		items[index].find('ul').append(item);
		callbacks[index].push(callback);
	}
	this.init = function(){
		var item;
		jQuery('.menu ul').html('');
		for(var i in items){
			item = items[i].appendTo('.menu ul');
			item.find('li').each(function(dex,dat){
				jQuery(this).on('click',callbacks[i][dex]);
			});
		}
	}
}










// function menu(){
// 	var fileMenu;

// 	function subMenuFile(){
// 		var fileMenu = new air.NativeMenu();
// 		fileMenu.addEventListener(air.Event.SELECT, function(){
// 			//nothing
// 		});

// 		var returnTitle = fileMenu.addItem(new air.NativeMenuItem('Return to Title'));
// 		returnTitle.addEventListener(air.Event.SELECT, function(){
// 			dialog('Are you sure?','Returning to title will lose any unsaved progress',function(){
// 				var title = new TitleClass();
// 				title.display();
// 			});
// 		});
// 		var newGame = fileMenu.addItem(new air.NativeMenuItem('New Game'));
// 		newGame.addEventListener(air.Event.SELECT, function(){
// 			message('Feature not yet available','This feature will be added soon, be sure to check back');
// 		});
// 		var loadGame = fileMenu.addItem(new air.NativeMenuItem('Load Game'));
// 		loadGame.addEventListener(air.Event.SELECT, function(){
// 			message('Feature not yet available','This feature will be added soon, be sure to check back');
// 		});
// 		var quit = fileMenu.addItem(new air.NativeMenuItem('Quit'));
// 		quit.addEventListener(air.Event.SELECT, function(){
// 			dialog('Are you sure?','Closing the game will lose any unsaved progress',function(){
// 				window.nativeWindow.close();
// 			});
// 		});
// 		return fileMenu;
// 	}

// 	if(air.NativeWindow.supportsMenu && nativeWindow.systemChrome != air.NativeWindowSystemChrome.NONE){
// 		nativeWindow.menu = new air.NativeMenu();
// 		nativeWindow.menu.addEventListener(air.Event.SELECT, function(){
// 			//nothing
// 		});
// 		fileMenu = nativeWindow.menu.addItem(new air.NativeMenuItem('File')); 
// 		fileMenu.submenu = subMenuFile();
// 	}
// 	if(air.NativeApplication.supportsMenu){
// 		var application = air.NativeApplication.nativeApplication; 
// 		application.menu.addEventListener(air.Event.SELECT, function(){
// 			//nothin
// 		}); 
//         fileMenu = application.menu.addItem(new air.NativeMenuItem('File')); 
//         fileMenu.submenu = subMenuFile();
// 	}
// }