function DBClass(){ //saves data to memory, not to disk
	var fn = this;

	if(!window.DBStorage){
		window.DBStorage={};
	}
	function _getEngine(){
		var engine;
		if(typeof(Storage)!=='undefined'){
			engine = 'localstorage';
		}else{
			engine = 'memory';
		}
		return engine;
	}
	function _getSessionStorage(key){
		return sessionStorage.getItem(key);
	}
	function _setSessionStorage(key,value){
		return sessionStorage.setItem(key,value);
	}
	function _getWindowStorage(key){
		if(window.DBStorage[key]){
			return window.DBStorage[key];
		}
		return false;
	}
	function _setWindowStorage(key,value){
		window.DBStorage[key] = value;
		return true;
	}
	function _get(key){
		var value;
		switch(_getEngine()){
			case 'localstorage':
				value = _getSessionStorage(key);
				break;
			default:
				value = _getWindowStorage(key);
				break;
		}
		if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
			replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
			replace(/(?:^|:|,)(?:\s*\[)+/g, ''))){
			return JSON.parse(value);
		}else{
			return value;
		}
	}
	function _set(key,value){
		if(value instanceof Object || value instanceof Array){
			value = JSON.stringify(value);
		}
		switch(_getEngine()){
			case 'localstorage':
				_setSessionStorage(key,value);
				break;
			default:
				_setWindowStorage(key,value);
				break;
		}
		return true;
	}
	this.get = function(key){
		return _get(key);
	}
	this.set = function(key,value){
		return _set(key,value);
	}
	return this;
}