function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}
function TRImage(src){
	var state = 'image-'+new Date().getTime()+Math.floor((Math.random()*10000)+1),
		fn = this,
		image,
		callbacks=[];
	filer.ls('/images',function(entries){
		var list = [];
		for(var i=0;i<entries.length;i++){
			list.push(entries[i].fullPath);
		}
		if(list.indexOf('/images/'+src.type+'-'+src.key+'.png')===-1){
			socket.emit('image',{
				state:state,
				src:src
			});
			socket.once(state,function(data){
				filer.write('/images/'+src.type+'-'+src.key+'.png',{data:b64toBlob(data,'image/png')},function(fileEntry,fileWriter){
					image = new Image();
					image.onload = function(){
						for(var i=0;i<callbacks.length;i++){
							callbacks[i](image);
						}
					}
					image.src = fileEntry.toURL();
				});
			});
		}else{
			image = new Image();
			image.onload = function(){
				for(var i=0;i<callbacks.length;i++){
					callbacks[i](image);
				}
			}
			image.src = entries[list.indexOf('/images/'+src.type+'-'+src.key+'.png')].toURL();
		}
	});
	this.ready = function(callback){
		callbacks.push(callback);
	}
}