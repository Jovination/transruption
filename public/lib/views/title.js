function TitleClass(){
	var fn = this,
		width = jQuery('#container').width(),
		height = jQuery('#container').height();
	this.createStage = function(){
		var system = new SystemClass();
		system.clear();
		window.stage = new Kinetic.Stage({
			container:'container',
			width:width,
			height:height
		});
		return window.stage;
	};
	this.display = function(){
		var stage = fn.createStage(),
			layer = new Kinetic.Layer(),
			group,backround;

		group = new Kinetic.Group({

		});

		var loader = new LoaderClass(config.title.background);
		loader.complete(function(images){
			background = new Kinetic.Image({
				image:images.main,
				width:width,
				height:height
			});

			new button(group,{
				x:(width/2)-362,
				y:(height/2)+50,
				width:360,
				text:'New Game',
				click:function(){
					var character = new CharacterClass();
					character.newGame();
				}
			});
			new button(group,{
				x:(width/2)+2,
				y:(height/2)+50,
				width:360,
				text:'Load Game',
				click:function(){
					message('Feature unavailable','This feature will be added soon.');
				}
			});
			new button(group,{
				x:(width/2)-362,
				y:(height/2)+82,
				width:360,
				text:'Login',
				click:function(){
					message('Feature unavailable','This feature will be added soon.');
				}
			});
			new button(group,{
				x:(width/2)+2,
				y:(height/2)+82,
				width:360,
				text:'Credits',
				click:function(){
					message('Feature unavailable','This feature will be added soon.');
				}
			});
			layer
				.add(background)
				.add(group);
			stage.add(layer);
		});
	}
}