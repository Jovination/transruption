function CharacterClass(){
	var fn = this,
		mouseX = 0,
		mouseY = 0,
		offsetX = 0,
		width = jQuery('#container').width(),
		height = jQuery('#container').height();
	this.createStage = function(){
		var system = new SystemClass();
		system.clear();
		window.stage = new Kinetic.Stage({
			container:'container',
			width:width,
			height:height
		});
		return window.stage;
	};
	this.characterScreen = function(stage){
		var layer = new Kinetic.Layer(),
			back = new Kinetic.Rect({
				width:config.characterCreation.preview.width,
				height:height,
				fill:config.characterCreation.preview.fill
			});
		layer.add(back);
		stage.add(layer);
		offsetX = config.characterCreation.preview.width;
	}
	this.newGame = function(){
		var stage = fn.createStage(),
			layer = new Kinetic.Layer(),
			group,back;
		fn.characterScreen(stage);
		group = new Kinetic.Group({
			x:offsetX,
			width:width-offsetX
		});
		back = new Kinetic.Rect({
			width:width-offsetX,
			height:height,
			fill:config.characterCreation.settings.fill
		});
		group.add(back);
		layer.add(group);
		stage.add(layer);

		var loader = new LoaderClass(config.forms.naming.icons);
		loader.complete(function(images){
			var creationForm = new FormsClass(group,layer);
			creationForm.naming(images,{id:'root'}).selector({
					active:0,
					id:'race',
					children:[{
						label:'Human',
						text:'The most abundant race on Talan and the least corrupted. they have kept their social structure together through isolationism.'
					},{
						label:'Elf',
						text:'Hidden from much of the world, the elfs can be found deep in the forests of Talan.'
					},{
						label:'Halfling',
						text:'Generally shy, they can be seen as weak but that is far from the truth.'
					}]
				}).selector({
					active:1,
					id:'build',
					children:[{
						label:'Strong',
						text:'lorem'
					},{
						label:'Average',
						text:'lorem'
					},{
						label:'Petite',
						text:'lorem'
					}]
				}).colorSwitch({
					text:'Body Color',
					id:'body',
					children:[{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'black'}]
				}).colorSwitch({
					text:'Hair Color',
					id:'hair',
					children:[{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'black'}]
				}).style({
					id:'hair',
					children:[{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'white',},{fill:'black'}]
				}).ready(function(form){
					new button(group,{
						x:group.getWidth()-367,
						y:height-35,
						width:360,
						text:'Start',
						click:function(){
							if(!jQuery('#name-select').val()){
								alert('You must enter a name!');
							}else{
								var main = new MainClass();
								main.startGame();
							}
						}
					});

					form.on('change',function(data){
						var character = {
							colors:{
								body:data.colorswitch.body,
								hair:data.colorswitch.hair
							},
							name:data.naming.root.input,
							gender:data.naming.root.active,
							build:data.selector.build,
							race:data.selector.race,
							tf:{
								hair:data.style.hair
							}
						};
						db.set('character',character);
					});
				});
			group.draw();
		});
	}
}