function MainClass(){
	var fn = this,
		mouseX = 0,
		mouseY = 0,
		offsetX = 0,
		width = jQuery('#container').width(),
		height = jQuery('#container').height(),
		events = new EventsClass();
	this.createStage = function(){
		var system = new SystemClass();
		system.clear();
		window.stage = new Kinetic.Stage({
			container:'container',
			width:width,
			height:height
		});
		return window.stage;
	};
	this.startGame = function(){
		var stage = fn.createStage(),
			background = new BackgroundClass,
			text = new TextClass,
			npc = new NpcClass,
			map = new MapClass;
		stage
			.add(background.getLayer())
			.add(npc.getLayer())
			.add(text.getLayer())
			.add(map.getLayer());
		background.set(2);
		npc.set(1,1);
		background.ready(function(){
		});	
		npc.ready(function(){
		});
		map.ready(function(){
		});	

	}
}