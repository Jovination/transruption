//requirements
var express = require('express'),
	fs = require('fs'),
	r = require('rethinkdb');

//globals
var app = express(),
	server = app.listen(8080),
	io = require('socket.io').listen(server,{log:false});

app.configure(function(){
	app.set('title', 'Transruption: The age of change');
	app.set('views',__dirname);
	app.set('view engine','jade');
	app.use(express.static(__dirname+'/public'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
});

app.get('/',function(req,res){
	res.render('index',{title:'Transruption: The age of change'});
});

var connection = null;
r.connect({host:'localhost',port:28015},function(err,conn){
	if(err) throw err;
	connection = conn;

	io.sockets.on('connection',function(socket){
		socket.on('image',function(data){
			fs.readFile(__dirname+'/images/'+data.src.type+'/'+data.src.key,function(error,data2){
				socket.emit(data.state,data2.toString());
			});
		});
	});
});